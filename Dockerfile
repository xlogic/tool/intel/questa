# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

# Base image:
# https://catalog.redhat.com/software/containers/ubi9/618326f8c0d15aff4912fe0b
ARG BASE_IMAGE_NAME=registry.access.redhat.com/ubi9
ARG BASE_IMAGE_TAG=9.3-1361.1699548029

FROM ${BASE_IMAGE_NAME}:${BASE_IMAGE_TAG}

# http://label-schema.org/rc1/
LABEL \
    maintainer="tymoteusz.blazejczyk@tymonx.com" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.version="${QUESTA_VERSION:-latest}" \
    org.label-schema.url="https://gitlab.com/xlogic/tool/intel/questa" \
    org.label-schema.name="intel-questa" \
    org.label-schema.usage="https://gitlab.com/xlogic/tool/intel/questa" \
    org.label-schema.vendor="xlogic.dev" \
    org.label-schema.vcs-url="https://gitlab.com/xlogic/tool/intel/questa" \
    org.label-schema.description="Questa*-Intel® FPGA Edition Software" \
    org.label-schema.docker.cmd="docker run -it --rm registry.gitlab.com/xlogic/tool/intel/questa" \
    org.label-schema.podman.cmd="podman run -it --rm registry.gitlab.com/xlogic/tool/intel/questa"

ENV \
    PATH="/opt/questa/bin:/opt/questa/linux_x86_64/mgls/bin:${PATH}" \
    MODEL_TECH="/opt/questa/bin" \
    LM_LICENSE_FILE="@xlogic-flexlm-intel-questa:27000"

RUN \
    dnf --assumeyes install libX11 libXext libXft gcc g++ python3 python3-devel cmake && \
    dnf clean all && \
    rm --recursive --force /var/cache/yum && \
    ln --symbolic /lib64/ld-linux-x86-64.so.2 /lib64/ld-lsb-x86-64.so.3 && \
    echo "questa:x:1000:1000::/var/questa:/bin/bash" >> /etc/passwd && \
    echo "questa:x:1000:" >> /etc/group && \
    mkdir --parent      /var/questa /var/run/questa && \
    chown questa:questa /var/questa /var/run/questa && \
    echo "d /var/run/questa 0750 questa questa -" > /usr/lib/tmpfiles.d/questa.conf

COPY install.sh /tmp/

ARG QUESTA_VERSION=23.3.104
ARG QUESTA_URL=https://downloads.intel.com/akdlm/software/acdsinst
ARG QUESTA_FILE=/tmp/questa.run
ARG QUESTA_MINIMAL=1

RUN \
    /bin/bash /tmp/install.sh \
        ${QUESTA_MINIMAL:+--minimal} \
        --url "${QUESTA_URL}" \
        --version "${QUESTA_VERSION}" \
        --file "${QUESTA_FILE}" \
        --output /opt/questa && \
    (rm --force /tmp/install.sh "${QUESTA_FILE}" 2>/dev/null || true)

COPY --chmod=0755 --chown=root:root entrypoint.sh /usr/local/bin/
COPY --chmod=0666 --chown=root:root modelsim.ini  /opt/questa/

USER questa:questa
WORKDIR /var/questa

CMD ["vsim", "-batch"]
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
