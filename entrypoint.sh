#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

set -e

main() {
    case "$1" in
        -*|+*)
            set -- vsim "$@";;
        *)
            ;;
    esac

    exec "$@"
}

main "$@"
