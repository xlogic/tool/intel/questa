#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

set -e

OUTPUT="."
URL="https://downloads.intel.com/akdlm/software/acdsinst"
VERSION="23.3.104"
FILE="./questa.run"
EDITION="lite"
MINIMAL=""

print_help() {
    cat <<-EOF
Usage: ./install.sh [OPTIONS]

It downloads and installs Questa*-Intel® FPGA Edition Software.

Options:
  -h, --help                it prints this help message.
  -m, --minimal             make installation minimal.
  -u, --url URL             download URL location.           Default: [${URL}]
  -f, --file FILE           archived file name to download.  Default: [${FILE}]
  -o, --output DIR          installation directory location. Default: [${OUTPUT}]
  -e, --edition NAME        edition: lite, standard or pro.  Default: [${EDITION}]
  -v, --version VERSION     version to download.             Default: [${VERSION}]
EOF
}

fatal() {
    >&2 echo "$*. Exiting..."; exit 1
}

normalize() {
    echo "$1" | tr '[[:upper:]]' '[[:lower:]]'
}

parse_command_line_arguments() {
    while [[ "$#" -gt 0 ]]; do
        case "$1" in
            -h|--help)
                print_help; exit 0;;
            -u|--url)
                URL="${2:-${URL}}"; shift;;
            -f|--file)
                FILE="${2:-${FILE}}"; shift;;
            -o|--output)
                OUTPUT="${2:-${OUTPUT}}"; shift;;
            -e|--edition)
                EDITION="$(normalize "${2:-${EDITION}}")"; shift;;
            -v|--version)
                VERSION="$(normalize "${2:-${VERSION}}")"; shift;;
            -m|--minimal)
                MINIMAL=1;;
            *)
                fatal "Unsupported option: $1";;
        esac

        shift
    done
}

get_url_lite() {
    case "$1" in
        ""|latest|23|23.3|23.3.104)
            echo "${URL}/23.3/104/ib_installers/QuestaSetup-23.3.0.104-linux.run";;
        22|22.1|22.1.1)
            echo "${URL}/22.1std.1/917/ib_installers/QuestaSetup-22.1std.1.917-linux.run";;
        *)
            fatal "Unsupported Quartus version: $1";;
    esac
}

download_installer() {
    if [[ "$(type -t "get_url_${EDITION}")" != function ]]; then
        fatal "Unsupported edition: ${EDITION}"
    fi

    url="$(eval "get_url_${EDITION}" "${VERSION}")"

    echo "Downloading installer: ${url}"
    curl --silent --location --output "$1" "${url}"
    echo "Downloaded installer"
}

minimal() {
    echo "Removing unwanted content"

    # Remove additional compiled libraries
    find "${OUTPUT}" -mindepth 1 -maxdepth 1 -type d -not \( \
        -name lib      -or \
        -name include  -or \
        -name std      -or \
        -name sv_std   -or \
        -name ieee     -or \
        -name linux_x86_64 \
    \) -print0 | xargs -0 -I{} rm --recursive --force "{}"

    # Remove static libraries
    find "${OUTPUT}/linux_x86_64" -type f -name '*.a' -print0 | xargs -0 -I{} rm --recursive --force "{}"

    ln --symbolic "${OUTPUT}/linux_x86_64" "${OUTPUT}/bin"
}

main() {
    parse_command_line_arguments "$@"

    mkdir --parent "${OUTPUT}"

    if [[ ! -f "${FILE}" ]]; then
        download_installer "${FILE}"
    else
        echo "Using local installer: ${FILE}"
    fi

    chmod u+x "${FILE}" 2>/dev/null || true

    echo "Installing Questa"
    "${FILE}" \
        --unattendedmodeui none \
        --mode unattended \
        --installdir "${OUTPUT}" \
        --accept_eula 1 \
        --questa_edition questa_fse

    mv "${OUTPUT}"/questa_fse/* "${OUTPUT}"

    if [[ -n "${MINIMAL}" ]]; then
        minimal
    fi

    # Fix permissions for others
    find "${OUTPUT}" -type f -print0 | xargs -0 -I{} chmod u+rw,g+r,o+r "{}"
    find "${OUTPUT}" -type f -executable -print0 | xargs -0 -I{} chmod u+rwx,g+rx,o+rx "{}"
    find "${OUTPUT}" -type d -print0 | xargs -0 -I{} chmod u+rwx,g+rx,o+rx "{}"
}

main "$@"
